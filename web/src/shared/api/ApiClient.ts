import axios from "axios";

// Get token from local storage
const currentToken = async () => {
  return localStorage.getItem("token");
};

// Create axios instance
const ApiClient = axios.create({
  baseURL: process.env.REACT_APP_API_HOST,
  timeout: 30000,
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
  },
});

// Add a request interceptor
ApiClient.interceptors.request.use(
  async function (config) {
    const token = await currentToken();

    if (token && config.headers) {
      config.headers.Authorization = `${token}`;
    }

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default ApiClient;
