import { combineReducers, configureStore } from "@reduxjs/toolkit";

import fetchAuth from "../../AuthSlice";
import fetchLogin from "../../feature/Login/LoginSlice";
import { useDispatch } from "react-redux";

// Collect all reducers
const reducers = {
  fetchLogin,
  fetchAuth,
};

// Combine all reducers
const combinedReducer = combineReducers(reducers);

const reducer = (state: any, action: any) => {
  // For logout action, reset the state to undefined
  if (action.type === "login/logout") {
    state = undefined;
  }
  return combinedReducer(state, action);
};

// Create store to collect all reducers
export const store = configureStore({
  reducer,
  devTools: process.env.NODE_ENV === "development",
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
