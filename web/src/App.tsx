import "./App.css";

import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
import { Layout, Spin } from "antd";
import { fetchAuthen, isSessionExpire } from "./AuthSlice";
import { useEffect, useState } from "react";

import { Content } from "antd/es/layout/layout";
import Dashboard from "./feature/Dashboard/Dashboard";
import Login from "./feature/Login/Login";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";

// wrap component with ProtectedRoute
const ProtectedRoute = ({ children, isExpire }: any) => {
  if (!isExpire) {
    return <Navigate to="/" replace />;
  }
  return children;
};

const RoutesComponent = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const selectorIsSessionExpire = useSelector(isSessionExpire);
  const [isLoaded, setIsLoaded] = useState(false);

  const fetchAuthAsync = async () => {
    await dispatch(fetchAuthen() as any);
    setIsLoaded(true);
  };

  useEffect(() => {
    fetchAuthAsync();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  return (
    <>
      {/* Wait until load check session expire complete */}
      {isLoaded ? (
        <Routes>
          <Route
            path="/dashboard"
            element={
              <ProtectedRoute isExpire={selectorIsSessionExpire}>
                <Dashboard />
              </ProtectedRoute>
            }
          />
          <Route path="/" element={<Login />} />
        </Routes>
      ) : (
        <Spin />
      )}
    </>
  );
};

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Layout className="site-layout">
          <Content
            style={{
              background: "#ffffff",
            }}
          >
            <RoutesComponent />
          </Content>
        </Layout>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
