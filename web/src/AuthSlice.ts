import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import ApiClient from "./shared/api/ApiClient";
import { PROTECTED } from "./shared/api/ApiEndPoint";
import { RootState } from "./shared/store/store";

type Auth = {
  loading: boolean;
  isLoginSession: boolean;
};

const initialValues: Auth = {
  loading: false,
  isLoginSession: false,
};

// fetAuthen like protected api to check token is still slive
export const fetchAuthen = createAsyncThunk(
  "request/auth",
  async (_, { rejectWithValue }) => {
    const token = localStorage.getItem("token");
    try {
      const response = await ApiClient.get(PROTECTED, {
        headers: {
          Authorization: `${token}`,
        },
      });
      return response.data;
    } catch (err) {
      if (err instanceof Error) {
        return rejectWithValue(err.message);
      }
    }
  }
);

const fetchAuth = createSlice({
  name: "auth",
  initialState: initialValues,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAuthen.fulfilled, (state, action) => {
      state.loading = false;
      state.isLoginSession = true;
    });

    builder.addCase(fetchAuthen.rejected, (state) => {
      state.loading = false;
      state.isLoginSession = false;
    });

    builder.addCase(fetchAuthen.pending, (state) => {
      state.loading = true;
      state.isLoginSession = true;
    });
  },
});

export const isSessionExpire = (store: RootState) =>
  store.fetchAuth.isLoginSession;
export default fetchAuth.reducer;
