import "./style.scss";

import CustomTable from "./CustomTable";

function Dashboard() {
  return (
    <div className="page-content page-dashboard">
      <CustomTable />
    </div>
  );
}

export default Dashboard;
