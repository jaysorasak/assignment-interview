import "./style.scss";

import {
  Button,
  Col,
  Input,
  InputNumber,
  Modal,
  Row,
  Select,
  Table,
  Upload,
  message,
} from "antd";

import { DownloadOutlined } from "@ant-design/icons";
import Papa from "papaparse";
import { UploadOutlined } from "@ant-design/icons";
import { useState } from "react";

interface RowData {
  [key: string]: string;
}

const initialDataSource: RowData[] = [];
const initialColumns: any = [];

const CustomTable = () => {
  const [dataSource, setDataSource] = useState<RowData[]>(initialDataSource);
  const [columns, setColumns] = useState(initialColumns);
  const [newColumnName, setNewColumnName] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);

  // Action
  const [prefix, setPrefix] = useState("");
  const [postfix, setPostfix] = useState("");
  const [regexPattern, setRegexPattern] = useState("");
  const [replacement, setReplacement] = useState("");
  const [operation, setOperation] = useState("add");
  const [operationNumber, setOperationNumber] = useState(0);
  const [selectedColumns, setSelectedColumns] = useState<string[]>([]);
  const [concatenationSeparator, setConcatenationSeparator] = useState("");
  // End Action

  const handleAddColumn = () => {
    setIsModalVisible(true);
  };

  const handleSaveColumn = () => {
    const newColumn = {
      title: newColumnName,
      dataIndex: newColumnName,
      key: newColumnName,

      // Render the new column
      render: (text: string, record: RowData) => {
        let combinedText = "";

        // If selected columns is not empty, combine the text
        if (selectedColumns.length > 0) {
          combinedText = selectedColumns
            .map((columnName) => {
              let value = record[columnName];
              return value;
            })
            .join(concatenationSeparator);
        } else {
          // If selected columns is empty, use the current column
          combinedText = record[newColumnName];
        }

        // If regex pattern and replacement are not empty, replace the text
        if (regexPattern && replacement) {
          combinedText = combinedText.replace(
            new RegExp(regexPattern, "g"),
            replacement
          );
        }

        // Perform operation (add or subtract) on the combinedText
        if (!isNaN(operationNumber)) {
          const numericCombinedText = parseFloat(combinedText);
          if (!isNaN(numericCombinedText)) {
            combinedText =
              operation === "add"
                ? (numericCombinedText + operationNumber).toString()
                : (numericCombinedText - operationNumber).toString();
          }
        }

        // If prefix or postfix is empty, it will be ignored
        return <span>{`${prefix}${combinedText}${postfix}`}</span>;
      },
    };
    setColumns([...columns, newColumn]);
    setIsModalVisible(false);
  };

  const handleUpload = (file: File) => {
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = () => {
      // Parse CSV data
      const csvData = reader.result as string;
      const rows = csvData.split("\n");
      const csvHeaders = rows[0].split(",");

      // If the new column name is not empty, add the new column
      if (csvHeaders.includes(newColumnName)) {
        // Create new data source
        const csvRows = rows.slice(1).map((row) => {
          const rowCells = row.split(",");
          const rowData: RowData = {};
          csvHeaders.forEach((header, index) => {
            rowData[header] = rowCells[index];
          });
          return rowData;
        });

        // Create new column
        const newColumn = {
          title: newColumnName,
          dataIndex: newColumnName,
          key: newColumnName,
          render: (text: string, record: RowData) => (
            <span>{record[newColumnName]}</span>
          ),
        };

        // Check if the new column name is already in the columns
        if (!columns.some((col: any) => col.title === newColumnName)) {
          setColumns([...columns, newColumn]);
        }
        setDataSource(csvRows);
      } else {
        // If the new column name is empty, show error message
        message.error(
          `The uploaded CSV file does not contain the "${newColumnName}" column.`
        );
      }
    };
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const uploadProps = {
    beforeUpload: (file: File) => {
      handleUpload(file);
      return false;
    },
  };

  // Export to CSV by using PapaParse by selector data by class name
  const exportToCSV = () => {
    const table = document.querySelector(".ant-table-content table");

    // Typescript check is table is null
    if (!table) {
      console.error("Table not found");
      return;
    }

    // Get headers
    const headers = Array.from(table.querySelectorAll("th")).map((th) =>
      th.textContent?.trim()
    );

    // Get rows
    const rows = Array.from(table.querySelectorAll("tbody tr")).map((tr) =>
      Array.from(tr.querySelectorAll("td")).map((td) => td.textContent?.trim())
    );

    // Combine headers and rows into a single array
    const tableData = [headers, ...rows];
    const csvData = Papa.unparse(tableData);
    const csvBlob = new Blob([csvData], { type: "text/csv;charset=utf-8;" });
    const csvUrl = URL.createObjectURL(csvBlob);
    const link = document.createElement("a");
    link.href = csvUrl;
    link.setAttribute("download", "table_data.csv");
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <div>
      <Table<RowData> dataSource={dataSource} columns={columns} />
      <div className="wrap-upload-button">
        <Button style={{ marginRight: 12 }} onClick={handleAddColumn}>
          Add Column
        </Button>
        <Upload {...uploadProps}>
          <Button icon={<UploadOutlined />}>Upload CSV</Button>
        </Upload>
        <Button icon={<DownloadOutlined />} onClick={exportToCSV}>
          Export CSV
        </Button>
      </div>
      <Modal
        className="wrap-table-customize"
        title="Add Column"
        open={isModalVisible}
        onOk={() => handleSaveColumn()}
        onCancel={handleCancel}
      >
        <label>Column name</label>
        <Input
          value={newColumnName}
          onChange={(e) => setNewColumnName(e.target.value)}
          placeholder="Column name"
        />
        <label>Column Prefix</label>
        <Input
          value={prefix}
          onChange={(e) => setPrefix(e.target.value)}
          placeholder="Prefix (optional)"
        />
        <label>Column Postfix</label>
        <Input
          value={postfix}
          onChange={(e) => setPostfix(e.target.value)}
          placeholder="Postfix (optional)"
        />
        <Row gutter={12}>
          <Col span={12}>
            <label>Regex Pattern</label>
            <Input
              value={regexPattern}
              onChange={(e) => setRegexPattern(e.target.value)}
              placeholder="Regex Pattern (optional)"
            />
          </Col>
          <Col span={12}>
            <label>Replacement</label>
            <Input
              value={replacement}
              onChange={(e) => setReplacement(e.target.value)}
              placeholder="Replacement (optional)"
            />
          </Col>
        </Row>
        <Row gutter={12}>
          <Col span={12}>
            <label>Number to add/subtract</label>
            <InputNumber
              min={0}
              style={{ width: "100%" }}
              value={operationNumber}
              onChange={(e) => {
                setOperationNumber(Number(e));
              }}
              placeholder="Number to add/subtract (optional)"
            />
          </Col>
          <Col span={12}>
            <label>Operation</label>
            <Select
              style={{ width: "100%" }}
              value={operation}
              onChange={(value) => setOperation(value)}
              placeholder="Operation (add/subtract)"
            >
              <Select.Option value="add">Add</Select.Option>
              <Select.Option value="subtract">Subtract</Select.Option>
            </Select>
          </Col>
        </Row>

        <Row gutter={12}>
          <Col span={12}>
            <label>Concatenate Columns</label>
            <Select
              style={{ width: "100%" }}
              mode="multiple"
              value={selectedColumns}
              onChange={(value) => setSelectedColumns(value)}
              placeholder="Select columns to concatenate (optional)"
            >
              {columns.map((col: any) => (
                <Select.Option key={col.key} value={col.dataIndex}>
                  {col.title}
                </Select.Option>
              ))}
            </Select>
          </Col>
          <Col span={12}>
            <label>Concatenation separator</label>
            <Input
              value={concatenationSeparator}
              onChange={(e) => setConcatenationSeparator(e.target.value)}
              placeholder="Concatenation separator (optional)"
            />
          </Col>
        </Row>
      </Modal>
    </div>
  );
};

export default CustomTable;
