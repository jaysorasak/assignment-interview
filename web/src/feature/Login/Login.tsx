import "./style.scss";

import { Button, Form, Input, message } from "antd";
import { fetchLogin, fetchStatus } from "./LoginSlice";

import { LoginData } from "./LoginInterface";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

const onFinishFailed = (errorInfo: any) => {
  console.log("Failed:", errorInfo);
};

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  // Get fetchStatus from redux store
  const fetchStatusState = useSelector(fetchStatus);

  // Finish form store token to localstorage
  const onFinish = async (values: LoginData) => {
    const reponse = await dispatch(fetchLogin(values) as any);
    const token = reponse.payload.token;
    localStorage.setItem("token", token);
  };

  // Monitoring fetchStatusState from redux store
  useEffect(() => {
    if (fetchStatusState === "success") {
      navigate("/dashboard");
    } else if (fetchStatusState === "failed") {
      message.error("Login Failed");
    } else {
      return;
    }
  }, [fetchStatusState, navigate]);

  return (
    <div className="page-content page-login">
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button
            type="primary"
            htmlType="submit"
            loading={fetchStatusState === "pending" ? true : false}
          >
            Submit
          </Button>
        </Form.Item>
        <table className="wrap-admin-detail">
          <tr>
            <td>Username:</td>
            <td>user1</td>
            <td>Password:</td>
            <td>password1</td>
          </tr>
          <tr>
            <td>Username:</td>
            <td>user2</td>
            <td>Password:</td>
            <td>password2</td>
          </tr>
        </table>
      </Form>
    </div>
  );
}

export default Login;
