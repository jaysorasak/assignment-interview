import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import ApiClient from "../../shared/api/ApiClient";
import { LOGIN } from "../../shared/api/ApiEndPoint";
import { LoginData } from "./LoginInterface";
import { RootState } from "../../shared/store/store";

type LoginState = {
  loading: boolean;
  isLoginSession: boolean;
  fetchStatus: string;
};

type LoginUser = {
  email: string | null | undefined;
  role: string[];
};

const initialValues: LoginState = {
  loading: false,
  isLoginSession: false,
  fetchStatus: "idle",
};

// Get fetchLogin response from api
export const fetchLogin = createAsyncThunk(
  "request/login",
  async (dataLogin: LoginData, { rejectWithValue }) => {
    try {
      const response = await ApiClient.post(LOGIN, dataLogin);
      return response.data;
    } catch (err) {
      if (err instanceof Error) {
        return rejectWithValue(err.message);
      }
    }
  }
);

const login = createSlice({
  name: "login",
  initialState: initialValues,
  reducers: {
    loggedIn: (state: LoginState) => {
      state.isLoginSession = true;
    },
  },

  // Set fetchLogin state contain 3 step: pending, success, failed
  extraReducers: (builder) => {
    builder.addCase(fetchLogin.fulfilled, (state, action) => {
      state.loading = false;
      state.isLoginSession = true;
      state.fetchStatus = "success";
    });

    builder.addCase(fetchLogin.rejected, (state) => {
      state.loading = false;
      state.isLoginSession = false;
      state.fetchStatus = "failed";
    });

    builder.addCase(fetchLogin.pending, (state) => {
      state.loading = true;
      state.isLoginSession = false;
      state.fetchStatus = "pending";
    });
  },
});

export const { loggedIn } = login.actions;
export type { LoginUser };

// export state fetchStatus
export const fetchStatus = (store: RootState) => store.fetchLogin.fetchStatus;
export default login.reducer;
