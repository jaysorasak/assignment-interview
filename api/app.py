from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

import uuid
import datetime

app = Flask(__name__)
cors = CORS(app, resources={
    r"/*": {
        "origins": "*",
        "allow_headers": [
            "Content-Type",
            "Access-Control-Allow-Headers",
            "Authorization",
            "X-Requested-With"
        ],
        "methods": ["DELETE", "POST", "GET", "PUT", "PATCH", "OPTIONS"]
    }
})

# Memory storage for users
users = {
    "user1": "password1",
    "user2": "password2"
}

#  storage for active sessions
sessions = {}

# Login endpoint


@app.route('/login', methods=['POST'])
def login():
    username = request.json.get('username')
    password = request.json.get('password')

    if not username or not password:
        return jsonify({'message': 'Username and password are required'}), 400

    if username not in users or users[username] != password:
        return jsonify({'message': 'Invalid username or password'}), 401

    # Generate a token and store it in the sessions dictionary
    token = str(uuid.uuid4())
    expires_at = datetime.datetime.now() + datetime.timedelta(minutes=30)
    sessions[token] = {'username': username, 'expires_at': expires_at}

    return jsonify({'token': token}), 200

# Protected API endpoint


@app.route('/api', methods=['GET'])
def api():
    token = request.headers.get('Authorization')

    if not token or token not in sessions:
        return jsonify({'message': 'Unauthorized'}), 401

    session = sessions[token]

    if session['expires_at'] < datetime.datetime.now():
        del sessions[token]
        return jsonify({'message': 'Session expired'}), 401

    # Refresh session expiration time
    session['expires_at'] = datetime.datetime.now() + \
        datetime.timedelta(minutes=30)

    return jsonify({'message': 'Hello, {}! This is a protected API request.'.format(session['username'])}), 200


if __name__ == '__main__':
    app.run(debug=True)
